# WhatsApp-broadens-in-app-business-directory-and-search-features
WhatsApp is introducing new Yellow Pages-like features to help users find businesses from within the instant messaging app, part of the Meta-owned platform’s growing attempts to make deeper inroads with e-commerce.

The encrypted messaging service, used by over 2 billion users worldwide, said on Thursday that it’s expanding a feature called ‘Directory’ to all users in the key overseas market of Brazil to help them browse and discover local small businesses in their neighborhoods. The nationwide rollout follows WhatsApp testing the directory feature in Sao Paulo last year.

WhatsApp is also introducing the ability to find larger businesses from within the app. The feature – rolling out in several markets (Brazil, Colombia, Indonesia, Mexico and the U.K.), a spokesperson told TechCrunch – will allow users to browse businesses by category such as banking, food and drink and travel as well as by their names.

The feature, called ‘Business Search,’ aims to help individuals avoid having to spend time looking for phone numbers of businesses from their websites and keying in and saving those details to their phone contacts, the company said at a WhatsApp-focused business summit in Brazil.

The new features underscore WhatsApp’s growing attempts to turn the behemoth messaging app into a commerce engine, one of its largest bets to generate revenue from the otherwise free service. The company disclosed in the quarterly earnings last month that the click-to-WhatsApp ads business had grown 80% year-over-year and was on track to generate $1.5 billion in annual revenue.

“We want to make it easier for people to get more done on WhatsApp,” Meta CEO Mark Zuckerberg said at the summit. “Part of that is building better ways to engage with businesses. And while millions of businesses in Brazil use it for chat, we haven’t made it easy to discover businesses or buy from them, so people end up having to use work-arounds. The ultimate goal here is to make it so you can find, message and buy from a business all in the same WhatsApp chat.”

Brazil, the most populous nation in Latin America, is a key region for WhatsApp. The platform, which has amassed over 120 million users in Brazil, has chosen the South American market for testing several new business offerings.

WhatsApp last year introduced a payments-to-merchant service in Brazil, in what was briefly a world-first feature for WhatsApp. It rolled back the functionality shortly afterwards following the local central bank stating that adequate risk and regulatory tests were needed to be undertaken first.

Brazil’s monetary authority said at the time that its decision would “preserve an adequate competitive environment, that ensures the functioning of a payment system that’s interchangeable, fast, secure, transparent, open and cheap.”

The encrypted messaging platform, which received the approval to operate peer-to-peer payments in the nation last year, said it’s still waiting for the regulatory clearance on merchant payments. But that’s not stopping it from continuing some development work.

Payments giant Cielo, multinational Fiserv, merchants acquirer Getnet, payments platform Mercado Pago and credit and debit cards player Rede have built the technical integration with WhatsApp and many of them are participating in production testing, Meta-owned unit said.

“If you run a business in Brazil, that means people will be able to find you, contact you and purchase from you all in one WhatsApp chat, and we’re working to bring this experience to more countries in the coming months too,” Zuckerberg said. “This is the next step for business messaging and I’m looking forward to hearing about the opportunities this unlocks for all of you.
